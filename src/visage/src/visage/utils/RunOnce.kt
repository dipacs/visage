package visage.utils

import kotlin.browser.window

class RunOnce(private val block: () -> Unit) {

    private var pending = false;

    fun run() {
        if (this.pending) {
            return;
        }
        this.pending = true

        window.setTimeout({
            this.pending = false
            this.block()
        }, 5)
    }
}
