package visage.dom

import visage.core.Components

class CDiv: ATag<DivAttributes, DivStyles, DivEvents>("div", DivAttributes(), DivStyles(), DivEvents())

class DivAttributes: TagAttributes()

class DivStyles: TagStyles()

class DivEvents: TagEvents()

fun Components.div(init: CDiv.() -> Unit = {}) = this.registerComponent(CDiv(), init)
